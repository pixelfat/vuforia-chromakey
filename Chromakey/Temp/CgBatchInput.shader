﻿// Adapted by Dan T. for use with Vuforia video playback sample
// Requires: Android 4.0 (ICS) or above
Shader "Crossclicks/Chromakey" {

    Properties {
    
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Color ("Color", Color) = (0.0, 1.0, 0.0, 1.0)
        _Cutoff ("Cutoff", Range (0,1)) = .5
        _Smooth ("Smooth", Range (0,1)) = .1
        
    }
    
    SubShader {

        Tags { "RenderType"="Transparent" }
        
        LOD 200
        
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off // Render both front and back facing polygons.
        // Lighting off
        
        CGPROGRAM
        #pragma surface surf Lambert

        uniform sampler2D _MainTex;
        uniform float4 _Color;
        uniform float _Cutoff;
        uniform float _Smooth;
        
        struct Input {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutput o) {
        
            half4 c = tex2D (_MainTex, IN.uv_MainTex);
            
            o.Albedo = c.rgb;
            
//            // simple chroma
//            if(c.g>c.r&&c.g>c.b) {
//                o.Alpha = 0.0;
//            } else {
//                o.Alpha = 1.0;
//            }
            
            // calculated chroma
            // Lifted from - https://github.com/BradLarson/GPUImage/blob/master/framework/Source/GPUImageChromaKeyFilter.m
            float maskY = 0.2989 * _Color.r + 0.5866 * _Color.g + 0.1145 * _Color.b;
            float maskCr = 0.7132 * (_Color.r - maskY);
            float maskCb = 0.5647 * (_Color.b - maskY);

            float Y = 0.2989 * c.r + 0.5866 * c.g + 0.1145 * c.b;
            float Cr = 0.7132 * (c.r - Y);
            float Cb = 0.5647 * (c.b - Y);
             
            float blendValue = smoothstep(_Cutoff, _Cutoff + _Smooth, distance(float2(Cr, Cb), float2(maskCr, maskCb)));

            o.Alpha = blendValue;

            o.Albedo = float4(c.rgb * blendValue, 1.0 * blendValue);
             
        }
        
        ENDCG
        
    } 
    
    FallBack "Diffuse"
    
}
